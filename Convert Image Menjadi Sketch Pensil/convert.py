import cv2
image = cv2.imread("maqruez.jpg")
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
negative = cv2.bitwise_not(gray)
blur=cv2.GaussianBlur(negative, (21,21),0)
invert=cv2.bitwise_not(blur)
sketch=cv2.divide(gray, invert ,scale=256.0)
cv2.imwrite("sketch.png",sketch)